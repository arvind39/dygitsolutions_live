<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>News :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <!-- PAGE TOP -->
    <section class="page_banner">
      <img src="./images/news_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>News</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="news_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-4 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/news_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Latest News</h2> 
                      </div>
                      <div class="text_box">
                        <p>Latest News Will be Updated Shortly... </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  
  </div>
<!-- /CONTENT -->
<?php
require("footer.php");
?>
