<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Industries :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/industries_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Industries</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="industries_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/industries_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Our Industries
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions provides IT solutions and resources across multiple verticals in almost every industry. We understand the unique requirements of each industry and customize our services accordingly.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>            
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Healthcare 
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions helps healthcare payers and providers analyze healthcare software and information systems to improve operations, manage risk and compliance, and reduce IT complexity. Read a case study.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Financial Services
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions helps financial services clients improve time to market and increase revenue by streamlining business/IT processes, ensuring regulatory compliance and managing risk.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">									
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Manufacturing & Distribution 
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions helps manufacturing and distribution clients connect throughout their entire supply chain using transformative enterprise-wide IT services.</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
							<div class="ltr_box">
                <div class="row align-items-lg-center">									
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Retail 
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions helps retail clients achieve IT efficiency across the retail value chain using an array of end-to-end planning, supply chain and channel services. Read a case study.</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>           
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
