<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Web and Portal Development :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/web-and-portal_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>WEB & PORTAL DEVELOPMENT</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="web_portal_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/web-and-portal-img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <!-- <div class="tilte medium_title">
                        <h2>Values
                        </h2> 
                      </div> -->
                      <div class="text_box">
                        <p>We bring our deep expertise in developing and managing rich UI & Web 2.0 technologies enabled complex, high performance and reliable web applications & portals.</p>
                      <p>Websites are the corporate flagship of digital marketing efforts.Our principles of web design and development includes an effective selling message and a stronger call to action.</p>
											<p>We offer web and portal development services covering all web-related business needs including web applications development and modernization, customization of open-source solutions, web services building, mash-ups deployment, Intranets /Extranets development and systems integration.</p>
											<p>The team develops dynamic and secure websites that exceeds corporate goals and lend a strong support to sales strategies. The database driven websites can accommodate any amount of complex business logic, which we present in a user-friendly manner.</p>
											</div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Expertise - Our expertise include but not limited to the following:</h2> 
                      </div>
                      <div class="text_box mt-3 mt-md-0">
                        <ul class="list_style">
                          <li>Website Branding, Design & Development</li>
                          <li>Graphic Design</li>
													<li>Logo Design</li>
                          <li>Brochures / Flyers</li>
													<li>Web Analytical Reports</li>
                          <li>Search Engine Optimization</li>													
													<li>Content Management Systems</li>
                          <li>User Interactive Forms Aiding Process Automation</li>
													<li>Business Intelligence Reports</li>                         
                        </ul>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
							<div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Social Media Marketing</h2> 
                      </div>
                      <div class="text_box mt-3 mt-md-0">
												<p>Search engine optimization is not complete without social media advertising, and other online marketing services. Our services include optimizing the social media for your website:</p>
                        <ul class="list_style">
                          <li>Identification of your advertising goals and objectives</li>
													<li>Analysis of your target audience</li>
													<li>Creation of targeted strategic content</li>
													<li>Spreading the content through various online social media such as YouTube, Facebook, Twitter, Blogs, LinkedIn</li>
													<li>Campaign monitoring, measuring, and reporting the results.</li>
                        </ul>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
							<div class="ltr_box pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Technologies</h2> 
                      </div>
                      <div class="text_box mt-3 mt-md-0">
                        <ul class="list_style">
                          <li><b>Front-end technologies include:</b> Photoshop, Flash, Dreamweaver, HTML, JavaScript</li>
													<li><b>Open Source Solutions include:</b> Wordpress, osCommerce</li>
													<li><b>Server Side technologies include:</b> ASP,.NET, JSP, XML, Php, perl etc.,</li>
													<li><b>Web servers that include:</b> Weblogic, Apache, Tomcat, MS IIS Server, ATG Dynamo, Jrun, Jboss</li>
													<li><b>Databases include:</b> Oracle, MS SQL Server, MYSQL, Sybase, Access, PostGre.</li>												                       
                        </ul>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<!-- /CONTENT -->
<?php
require("footer.php");
?>
