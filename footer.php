<section class="subscribe_sec py-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-xl-9 col-md-8">
        <div class="sebscribe_part">
          <h3>Ready for ideas and coffee?
          </h3>
          <p class="m-0">We're always on the lookout for clients who have big ideas, whether you're a start-up company with big ideas or an established 
            <strong>lifetime upgrade
            </strong> brand ready to make a big impact.
          </p>
        </div>
      </div>
      <div class="col-xl-3 col-md-4 text-md-end text-center mt-4 mt-md-0">
        <div class="apply_now">
          <a href="javascript:void(0)" class="cta white_cta" data-bs-toggle="modal" data-bs-target="#myModal">
            <span>Apply Online
            </span>
            <svg width="13px" height="10px" viewBox="0 0 13 10">
              <path d="M1,5 L11,5">
              </path>
              <polyline points="8 1 12 5 8 9">
              </polyline>
            </svg>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<footer id="footer">
  <div class="pt-5 pb-sm-5 pb-4 ">
    <div class="container">
      <div class="row">
        <div class="spaced col-md-4 col-sm-4">
          <h4> 
            <strong>Our Mission
            </strong>
          </h4>
          <p>
            To be the company of choice to help our clients increase their competitive edge by delivering business solutions through information technology. We emphasize clients on attaining their strategic business goals by delivering cost effective, value-driven integrated technology solutions.
          </p>
        </div>
        <div class="spaced col-md-8 col-sm-8 hidden-sm hidden-xs ps-xl-5 ps-lg-4  ">
          <div class="row">
            <div class="col-md-12">
              <h4>Information
              </h4>
            </div>
            <div class="col-md-4">              
              <ul class="list-unstyled nobordered">
                <li>
                  <a class="block" href="index.php">
                    <i class="fa fa-angle-right">
                    </i> Home
                  </a>
                </li>
                <li>
                  <a class="block" href="about-us.php">
                    <i class="fa fa-angle-right">
                    </i> About Us
                  </a>
                </li>
                <li>
                  <a class="block" href="mission-and-values.php">
                    <i class="fa fa-angle-right">
                    </i> Mission/Value
                  </a>
                </li>
                <li>
                  <a class="block" href="services.php">
                    <i class="fa fa-angle-right">
                    </i> Our Services
                  </a>
                </li>
                <li>
                  <a class="block" href="our-resources.php">
                    <i class="fa fa-angle-right">
                    </i> Our Resources
                  </a>
                </li>
                <li>
                  <a class="block" href="our-partners.php">
                    <i class="fa fa-angle-right">
                    </i> Our Partners
                  </a>
                </li>
              </ul>
            </div>
            <div class="col-md-4">            
              <ul class="list-unstyled nobordered">
                <li>
                  <a class="block" href="it-training.php">
                    <i class="fa fa-angle-right">
                    </i> IT Training
                  </a>
                </li>
                <li>
                  <a class="block" href="database-management.php">
                    <i class="fa fa-angle-right">
                    </i> Database Management
                  </a>
                </li>
                <li>
                  <a class="block" href="business-transformation.php">
                    <i class="fa fa-angle-right">
                    </i> Business Transformation
                  </a>
                </li>
                <li>
                  <a class="block" href="application-development.php">
                    <i class="fa fa-angle-right">
                    </i> Application Development
                  </a>
                </li>
                <li>
                  <a class="block" href="web-and-portal-development.php">
                    <i class="fa fa-angle-right">
                    </i> Web & Portal Development
                  </a>
                </li>
                <li>
                  <a class="block" href="consulting-services.php">
                    <i class="fa fa-angle-right">
                    </i> Consulting Services
                  </a>
                </li>
              </ul>
            </div>
            <div class="col-md-4">              
              <ul class="list-unstyled nobordered">
                <li>
                  <a class="block" href="approach.php">
                    <i class="fa fa-angle-right">
                    </i> Our Approach
                  </a>
                </li>
                <li>
                  <a class="block" href="news.php">
                    <i class="fa fa-angle-right">
                    </i> Latest News
                  </a>
                </li>
                <li>
                  <a class="block" href="industries.php">
                    <i class="fa fa-angle-right">
                    </i> Industries
                  </a>
                </li>
                <!--<li>-->
                <!--  <a class="block" href="javascript:void(0)">-->
                <!--    <i class="fa fa-angle-right">-->
                <!--    </i> Testimonials-->
                <!--  </a>-->
                <!--</li>-->
                <li>
                  <a class="block" href="careers-opportunities.php">
                    <i class="fa fa-angle-right">
                    </i> Careers
                  </a>
                </li>
                <li>
                  <a class="block" href="contact-us.php">
                    <i class="fa fa-angle-right">
                    </i> Contact Us
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr />
  <div class="copyright">
    <div class="container text-center fsize12">
      <p>
        All Right Reserved &copy; Dynamics Global. Powered by
        <a href="https://www.amazing7.com/" target="_blank" title="" class="copyright">Amazing 7 Studios. 
        </a>
        <a href="javascript:void(0)" class="fsize11"> Privacy Policy 
        </a>
        <a href="javascript:void(0)" class="fsize11"> Terms of Service
        </a>
      </p>
    </div>
  </div>
</footer>
<a href="#" id="toTop">
</a>
<div class="modal fade" id="myModal" tabindex="-1"  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered ">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title">
          Apply Online          
        </h2>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <script type="text/javascript">
          function formValidator()
          {
            var name=document.getElementById('name');
            var email=document.getElementById('email');
            var contact_subject=document.getElementById('contact_subject');
            var message=document.getElementById('message');
            if(isname(name, "Please enter only letters for your Name"))
            {
              if(isemail(email, "Please Enter Your Valid Email"))
              {
                if(isnum(contact_subject, "Please Enter Your Valid Contact No."))
                {
                  if(isname(message, "Please Enter Your Message"))
                  {
                    return true;
                  }
                }
              }
            }
            return false;
          }
          function isname(elem, helpermsg)
          {
            var alphaExp = /^[A-Za-z\\-\\., \']+$/;
            if(elem.value.match(alphaExp))
            {
              return true;
            }
            else
            {
              alert(helpermsg);
              elem.focus();
              return false;
            }
          }
          function isemail(elem, helpermsg)
          {
            var emailexp=/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-Z0-9]{2,4}$/;
            if(elem.value.match(emailexp))
            {
              return true;
            }
            else
            {
              alert(helpermsg);
              elem.focus();
              return false;
            }
          }
          function isnum(elem, helpermsg)
          {
            var phexp=/^[0-9\\-\\., \']+$/;
            if(elem.value.match(phexp))
            {
              return true;
            }
            else
            {
              alert(helpermsg);
              elem.focus();
              return false;
            }
          }
        </script>
        <form action="e_process1.php" method="post" id="contactform_main" class="p-0" onsubmit="return formValidator()">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group mb-2">                
                <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="input_field" name="name" id="name" placeholder="Full Name*" required>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="form-group mb-2">
                
                <input type="email" class="input_field" name="email" id="email" placeholder="E-mail Address *" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group my-2">               
                <input type="text" name="contact_subject" id="contact_subject" placeholder="Phone No. *" value="" data-msg-required="Please enter the subject." maxlength="100" class="input_field" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group my-2">
                                
                <input type="text" value="" class="input_field" name="address" id="address" placeholder="Address" data-msg-required="Please enter the your address." maxlength="100">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group my-2">      

                <textarea rows="2" class="input_field" name="message" id="message" placeholder="Message *" maxlength="1000" data-msg-required="Please enter your message."  required></textarea>
                
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-2">
                <input class="d-none" type="text" name="captcha" id="captcha" value="" />
                <!-- keep it hidden -->
                <input type="submit" name="Submit" value="Submit" class="submit_btn" id="contact_submit" />
              </div>
            </div>
          </div>
        </form>
        <link rel="stylesheet" href="css/reveal.css">	
        <script type="text/javascript" src="js/jquery.reveal.js">
        </script>
      </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript" src="engine1/jquery.js"> </script>-->
<script type="text/javascript" src="js/scripts.js">
</script>
<script src="js/owl.carousel.min.js">
</script>
<script>
  //Banner Slider
  $('#banner_slider').owlCarousel({
    loop: true,
    margin: 0,  
    nav: false,
    mouseDrag: true,
    autoplay: true,
    autoplayTimeout: 30000,
    animateOut: 'slideOutUp',
    dots: true,
    dotsData: false,
    responsive: {
      0: {
        items: 1
      }
      ,
      600: {
        items: 1
      }
      ,
      1000: {
        items: 1
      }
    }
    ,
  }
                                 );
</script>
<script>
  var dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'));
  var dropdownSubmenuElementList = [].slice.call(document.querySelectorAll('.dropdown-submenu-toggle'));
  var dropdownMenus = [].slice.call(document.querySelectorAll('.dropdown-menu'));
  var dropdownList = dropdownElementList.map(function (dropdownToggleEl) {
    return new bootstrap.Dropdown(dropdownToggleEl);
  }
                                            );
  var submenuList = dropdownSubmenuElementList.map(function(e) {
    e.onclick = function(e){
      e.target.parentNode.querySelector('ul').classList.toggle('show');
      e.stopPropagation();
      e.preventDefault();
    }
  }
                                                  );
  var masterClickEvent = document.addEventListener('click',function(e){
    // Function to remove show class from dropdowns that are open
    closeAllSubmenus();
    // Hamburger menu
    if(e.target.classList.contains('hamburger-toggle')){
      e.target.children[0].classList.toggle('active');
    }
  }
                                                  );
  function closeAllSubmenus(){
    // Function to remove show class from dropdowns that are open
    dropdownMenus.map(function (menu) {
      menu.classList.remove('show');
    }
                     );
  }
</script>
</body>
</html>
