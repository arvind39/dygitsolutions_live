<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Database Management :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <!-- PAGE TOP -->
		<section class="page_banner">
      <img src="./images/database_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Database Management</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="database_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/database_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Database Management</h2> 
                      </div>
                      <div class="text_box">
                        <p>With "Business Intelligence" utilization rapidly becoming a best practice the demand for database management and the supporting resources continues to grow. Database management allows companies to collect, securely store, analyze and then leverage their data to make fact based business decisions.</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Our staff can assist your business to:</h2> 
                      </div>
                      <div class="text_box mt-3 mt-md-0">
                       <p>Design, map, monitor, maintain, and tune databases for peak performance and proper data archives. Analyze data requirements and create and manage data models for existing or newly developed applications.
Utilize tools like SQL Server Reporting Services or Business Objects to create user friendly access and reports created from available data or newly captured data for the purpose of Business Intelligence.</p>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
             
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
	
  </div>

<?php
require("footer.php");
?>
