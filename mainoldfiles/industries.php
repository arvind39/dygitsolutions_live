<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Industries :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
		<section class="page-title img-responsive" style="background: url(images/industries.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Industries</strong> Us -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">INDUSTRIES</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/industries-short.jpg" alt="" class="float-left bordered">Dynamics Global IT Solutions provides IT solutions and resources across multiple verticals in almost every industry. We understand the unique requirements of each industry and customize our services accordingly.<br/></p>
                        <br/><br/><br/><br/><br/> <p class="just"><strong>Healthcare -</strong> Dynamics Global IT Solutions helps healthcare payers and providers analyze healthcare software and information systems to improve operations, manage risk and compliance, and reduce IT complexity. Read a case study.</p>
						   <p class="just"><strong>Financial Services -</strong> Dynamics Global IT Solutions helps financial services clients improve time to market and increase revenue by streamlining business/IT processes, ensuring regulatory compliance and managing risk.</p>
						    <p class="just"><strong>Manufacturing & Distribution -</strong> Dynamics Global IT Solutions helps manufacturing and distribution clients connect throughout their entire supply chain using transformative enterprise-wide IT services.</p>
							 <p class="just"><strong>Retail -</strong> Dynamics Global IT Solutions helps retail clients achieve IT efficiency across the retail value chain using an array of end-to-end planning, supply chain and channel services. Read a case study.</p>
						
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>