<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Services :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/our-services.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Our</strong> Services -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">OUR SERVICES</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/our_services-short.png" alt="" class="float-left bordered">Dynamics Global IT Solutions, LLC provides a wide range of software services. You name it and we build it!
For every client requirement varies. Dynamics Global IT Solutions, has carefully categorized the services from which client can easily identify the nature of service they want. Following are the various services offered by Dynamics Global IT Solutions that client can browse through.<br/></p>
                         <div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th colspan="3">VERTICALS</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Application Development</td>
											<td>Healthcare</td>
										</tr>
										<tr>
											<td>Web and Portal Development</td>
											<td>Manufacturing, Logistics & Distribution</td>
										</tr>
										<tr>
											<td>Mobile Development</td>
											<td>Telecom/Technology</td>
										</tr>
										<tr>
											<td>Testing Services</td>
											<td>Education</td>
										</tr>
										<tr>
											<td>Offshore Development</td>
											<td>Energy & Utilities</td>
										</tr>
										<tr>
											<td>Remote Database Administration</td>
											<td>RealEstate</td>
										</tr>
										<tr>
											<td>Enterprise Applications</td>
											<td></td>
										</tr>
										<tr>
											<td>Managed Services</td>
											<td></td>
										</tr>
										<tr>
											<td>Technology Consulting</td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>

							
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
						

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>