<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Values :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/mission-value.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Mission And Value</strong> Us -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">VALUES</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/img1.png" alt="" class="float-left bordered">Dynamics Global IT Solutions is committed to helping our customers meet their goals and achieve their missions. In line with our commitment, our core values are the principles that define our behavior. They shape our strategy and set our priorities. Internally, they guide our culture, and externally, they are fundamental to our success. Our values define how we interact with one another, our customers and our partners. They influence our strategic planning as well as our day-to-day decision making.<br/></p>
                         <br/><br/> <h4><strong>Integrity - We commit to the highest ethical standards in all that we do</strong></h4>
						   <ul class="list-icon spaced check-circle">
						   <li>We conduct our activities honestly and ethically.</li>
								<li>We earn our reputation as a highly ethical company through the integrity of our decisions and actions.</li>
						    </ul>
							<h4><strong>Respect - We encourage diversity of culture, background, experience, thoughts and ideas</strong></h4>
						  <p class="just">We demonstrate respect for our colleagues and customers through professionalism in our words and actions. We benefit as a team from our differences, where diversity contributes to innovative ideas and drives better results for our customers.</p>
						  
						  <h4><strong>100% Employee owned</strong></h4>
						<p class="just">Dynamics Global IT Solutions is 100% employee owned. Dynamics Global IT Solutions employee owners are committed to the common goal of delivering high-quality solutions to our customers. When you deal with anyone at any level of our company, you are dealing with an owner who puts the highest value on maintaining Dynamics Global IT Solutions reputation of trust by exceeding your expectations. Our integrity as employee owners demands that we are so trust worthy, your confidence in our ability to deliver is assured.
						</p>
					 <img src="images/comm1.jpg" alt="" class="float-right bordered">

                            <h4><strong>Trust - We follow through on commitments to our teammates and customers</strong></h4>
							<p  class="just">We earn our customer's trust by providing quality service and delivering on our promises, striving for continuous improvement. We maintain open and honest communications with our fellow employees, partners and customers.</p>
							<h4>Teamwork - We are "one team, one fight," working together to achieve our goals</h4>
							<p>We utilize resources, best practices and capabilities across our company to meet our customer's requirements. We contribute to the efficiency, growth and profitability of our company through dedication to teamwork.</p>
							<h4><strong>Mission - We make business decisions that are first and foremost based on achieving customer mission success</strong></h4>
							<p>We provide services and solutions that are relevant, innovative, timely and affordable-consistently exceeding customer expectations. We are proud to do meaningful work and help our customers achieve their goals in service to their constituents.</p>
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>