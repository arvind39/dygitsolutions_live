<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>About Us :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/about-us.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>About</strong> Us -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">DYNAMIC GLOBAL IT SOLUTIONS AT A GLANCE</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/about-us-short.jpg" alt="" class="float-left bordered">Dynamics Global IT Solutions is a global IT solutions and services provider employs a blended model of grander technology, domain and process expertise to increase our customers competitive advantage. At Dynamics Global IT Solutions, our experts put IT to work for you, applying custom solutions to your business problems quickly, efficiently and cost-effectively yet by following PMI, ITIL and CMMI standards. We pride on traditions of excellence in engineering and service delivery tied with high employee and customer satisfaction levels. Our unique hybrid-shore delivery model provides close onsite interaction with customers and a strong process oriented offshore team. At all levels, our employees continually interact to provide a superior outsourcing experience to customers. 100 percent employee owned.<br/></p>
                          <h4><strong>Our Mission</strong></h4>
						  <p class="just">To be the company of choice to help our clients increase their competitive edge by delivering business solutions through information technology. We emphasize clients to attain their strategic business goals by delivering cost effective, value-driven integrated technology solutions to seamlessly manage their business processes and products.</p>
						  
						  <h4><strong>Values</strong></h4>
						  <ul class="list-icon spaced check-circle">
								<li>Excellence and Quality in all we do.</li>
								<li>Service and Integrity in how we treat our clients, employees, candidates, investors, and suppliers.</li>
								<li>Ethical and Moral conduct at all times.</li>
								<li>Social Responsibility to communities we serve.</li>
								<li>Teamwork acknowledging that everyone has talent and skills that work together for excellence.</li>
								<li>Priorities resulting in a balanced life with faith, family, career, and personal development.</li>
							
							</ul>
                            <h4><strong>100% Employee owned</strong></h4>
							<p  class="just">Dynamics Global IT Solutions is 100% employee owned. Dynamics Global IT Solutions employee owners are committed to the common goal of delivering high-quality solutions to our customers. When you deal with anyone at any level of our company, you are dealing with an owner who puts the highest value on maintaining Dynamics Global IT Solutions reputation of trust by exceeding your expectations. Our integrity as employee owners demands that we are so trust worthy, your confidence in our ability to deliver is assured.</p>
							
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
						

</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>