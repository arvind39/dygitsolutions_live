<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Database Management :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/database-managment.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Database</strong> Management -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">DATABASE MANAGEMENT</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/database-short.jpg" alt="" class="float-left bordered">With "Business Intelligence" utilization rapidly becoming a best practice the demand for database management and the supporting resources continues to grow. Database management allows companies to collect, securely store, analyze and then leverage their data to make fact based business decisions.<br/></p>
						<br/><br/><br/><br/><h4><strong>Our staff can assist your business to:</strong></h4>
                        <p class="just">Design, map, monitor, maintain, and tune databases for peak performance and proper data archives.
Analyze data requirements and create and manage data models for existing or newly developed applications.<br/>Utilize tools like SQL Server Reporting Services or Business Objects to create user friendly access and reports created from available data or newly captured data for the purpose of Business Intelligence.</p>		
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>