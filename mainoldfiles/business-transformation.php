<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Business Transformation :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/business-transformtion.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Business</strong> Transformation -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;"><strong>BUSINESS TRANSFORMATION</strong></h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/business-short.png" alt="" class="float-left bordered"><h4>Business Process Management</h4>Our team does the heavy lifting to inject new efficiencies into your operations. At every step, we strive for simplicity and apply creativity to improve alignment to strategy and cut costs - while keeping an eye fixed on the best end-state scenarios.<br/></p>
						<br/><br/><br/><h4><strong>Project & Program Management</strong></h4>
                        <p class="just">Maximize the business value of your technology investments with major improvements in decision making, project speed to market, cost and performance.</p>
						<h4><strong>Information Security & IT Risk Management</strong></h4>
                        <p class="just">We help financial institutions and other regulated businesses protect their critical IT and information assets by providing information security management, data privacy, IT risk governance, and compliance management.</p>
				
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
						

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>