<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Resources :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/our-resources.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>Our Resources</strong> Us -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
			<?php require("sidebar.php"); ?>
<div class="col-md-9">

							   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">OUR RESOURCES</h2>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						
						<p class="just">	<img src="images/resource-short.png" alt="" class="float-left bordered">Dynamics Global IT Solutions has the resources and expertise that deliver maximum results for you. Whether you need temporary help, temp-to-full-time hire or full-time hires, we can quickly and effectively place talented people in the right jobs and ensure that you operate at maximum productivity.<br/></p>
                        <br/><br/><br/><br/>  <h4><strong>Industry professionals</strong></h4>
						  <p class="just">Our staffing professionals have years of experience in the industries they serve and an in-depth understanding of the workforce and the issues that most affect your business performance.</p>
						  
						  <h4><strong>Timeliness</strong></h4>
						 <p class="just">We are 100% dedicated to finding and placing talented workers. With a wide network of professionals, we are able to quickly provide the most qualified candidates when and where you need them.</p>
                            <h4><strong>Flexibility</strong></h4>
							<p  class="just">Our vast network includes professionals seeking a range of work arrangements. Whether you need a temporary employee for a project, a full-time addition to your staff, or anything in between, we can help. We are nimble enough to change as rapidly as your requirements do.</p>
							    <h4><strong>Customer service</strong></h4>
							<p  class="just">We gain an in-depth understanding of your business needs to ensure we deliver both exceptional service and results.</p>
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
						

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>