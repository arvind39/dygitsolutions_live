<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="Author" content="" />
<!-- mobile settings -->
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/demo/favicon.ico" />
<!-- WEB FONTS -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
<!-- CORE CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="assets/css/sky-forms.css" rel="stylesheet" type="text/css" />
<link href="css/weather-icons.min.css" rel="stylesheet" type="text/css" />
<link href="assets/css/line-icons.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/owl.pack.css" type="text/css" />
<link rel="stylesheet" href="css/owl.theme.default.min.css" type="text/css" />
<link href="css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/style2.css" />
<!--[if lt IE 9]>
<link href="css/bootstrap4ie8.css" rel="stylesheet">
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- THEME CSS -->

<link href="css/layout.css" rel="stylesheet" type="text/css" />
<link href="css/header-default.css" rel="stylesheet" type="text/css" />
<link href="css/footer-default.css" rel="stylesheet" type="text/css" />
<link href="css/color_scheme/red.css" rel="stylesheet" type="text/css" id="color_scheme" />
<!-- <link href="css/layout-dark.css" rel="stylesheet" type="text/css"  /> -->
<link href="css/revolution-slider.css" rel="stylesheet" type="text/css" />

<link href="css/essentials.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>

<script src="js/owl.carousel.js">
</script>
<!-- Morenizr -->
<script type="text/javascript" src="js/modernizr.min.js">
</script>
<!--[if lte IE 8]>
<script src="js/respond.js"></script>
<![endif]-->
<script>
  $('#myModal').reveal({
    animation: 'fadeAndPop',                   //fade, fadeAndPop, none
    animationspeed: 300,                       //how fast animtions are
    closeonbackgroundclick: true,              //if you click background will modal close?
    dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
  }
);
</script>
</head>
<body class="smoothscroll grey boxed pattern3">
  <div id="wrapper">
    <div id="header">
      <!-- class="sticky" for sticky menu -->
      <!-- Top Bar -->
      <header id="topBar">
        <div class="container">
          <div class="pull-right fsize13 margin-top10 hide_mobile">
            <!-- mail , phone -->
            <a href="mailto:admin@dygitsolutions.com">admin@dygitsolutions.com
            </a> &bull; 678-720-4916 &bull; 
            <a href="#"  class="btn btn-primary big-link" data-reveal-id="myModal" style="padding: 6px 16px !important;">Apply Online
            </a>
            <div class="block text-right">
              <!-- social -->
              <a href="https://www.facebook.com/dygitsolutions" class="social fa fa-facebook" target="_blank">
              </a>
              <a href="" class="social fa fa-twitter">
              </a>
              <a href="https://plus.google.com/u/0/b/116916338082283282017/116916338082283282017/posts/p/pub" class="social fa fa-google-plus" target="_blank">
              </a>
              <a href="https://www.linkedin.com/company/3781339" class="social fa fa-linkedin" target="_blank">
              </a>
            </div>
            <!-- /social -->
          </div>
          <!-- Logo -->
          <a class="logo" href="http://www.dygitsolutions.com/">
            <img src="images/logo.png" height="70" alt="" />
          </a>
        </div>
        <!-- /.container -->
      </header>
      <!-- /Top Bar -->
      <!-- Top Nav -->
      <header id="topNav">
        <div class="container">
          <!-- Mobile Menu Button -->
          <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
            <i class="fa fa-bars">
            </i>
          </button>
          <!-- Top Nav -->
          <div class="navbar-collapse nav-main-collapse collapse">
            <nav class="nav-main">
              
              <ul id="topMain" class="nav nav-pills nav-main">
                <li class="mega-menu dropdown">
                  <a href="index.php">
                    HOME								
                  </a>
                </li>
                <li class="dropdown mega-menu">
                  <a  href="about-us.php">ABOUT US
                  </a>
                </li>
                <li class="dropdown">
                  <a href="mission-and-values.php">VALUES
                  </a>
                </li>
                <li class="dropdown">
                  <a href="approach.php">APPROACH
                  </a>
                </li>
                <li class="dropdown mega-menu">
                  <a href="news.php">NEWS
                  </a>
                </li>                
                <li class="dropdown">
                  <a href="services.php">OUR SERVICES
                  </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="database-management.php">DATABASE MANEGEMENT
                      </a>
                    </li>
                    <li>
                      <a href="application-development.php">APPLICATION DEVELOPMENT
                      </a>
                    </li>
                    <li>
                      <a href="web-and-portal-development.php">WEB & PORTAL DEVELOPMENT
                      </a>
                    </li>
                    <li>
                      <a href="mobile-development.php">MOBILE DEVELOPMENT
                      </a>
                    </li>
                    <li>
                      <a href="remote-dba.php">REMOTE DBA
                      </a>
                    </li>
                    <li>
                      <a href="it-training.php">IT TRAINING & COACHING
                      </a>
                      <ul class="dropdown-menu">
                        <li>
                          <a href="corporate-training.php">Corporate Training
                          </a>
                        </li>
                        <li>
                          <a href="online-training.php">Online Training
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="business-transformation.php">BUSINESS TRANSFORMATION
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="our-resources.php">OUR RESOURCES
                  </a>
                </li>
                <li class="dropdown">
                  <a href="our-partners.php">PARTNERS
                  </a>
                </li>
                <li class="dropdown">
                  <a href="industries.php">INDUSTRIES
                  </a>
                </li>
                <li class="dropdown">
                  <a href="careers-opportunities.php">CAREERS
                  </a>
                </li>
                <li class="dropdown">
                  <a href="contact-us.php">CONTACT US
                  </a>
                </li>
              </ul>
            </nav>
          </div>
          <!-- /Top Nav -->
        </div>
        <!-- /.container -->
      </header>
      <!-- /Top Nav -->
    </div>
