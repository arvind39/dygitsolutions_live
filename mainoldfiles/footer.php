<!-- CALLOUT -->
<div class="callout dark">
  <!-- add "styleBackgroundColor" class for colored background and white text OR "dark" for a dark callout -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <!-- title + shortdesc -->
        <h3>Ready for ideas and coffee?
        </h3>
        <p>We're always on the lookout for clients who have big ideas, whether you're a start-up company with big ideas or an established 
          <strong>lifetime upgrade
          </strong> brand ready to make a big impact.
        </p>
      </div>
      <div class="col-md-3">
        <!-- button -->
        <a href="#"  class="btn btn-primary btn-lg big-link" data-reveal-id="myModal">Apply Online
        </a>
      </div>
    </div>
  </div>
</div>
<!-- /CALLOUT -->
<!-- FOOTER -->
<footer id="footer">
  <div class="container-fluid">
    <div class="row">
      <!-- col #1 -->
      <div class="spaced col-md-4 col-sm-4">
        <h4>Our 
          <strong>Mission
          </strong>
        </h4>
        <p>
          To be the company of choice to help our clients increase their competitive edge by delivering business solutions through information technology. We emphasize clients on attaining their strategic business goals by delivering cost effective, value-driven integrated technology solutions.
        </p>
        <h4>Phone: 678-720-4916
        </h4>
        <h4>Fax: 866-326-1422
        </h4>
        <p class="block">
          <!-- social -->
          <a href="https://www.facebook.com/dygitsolutions" class="social fa fa-facebook" target="_blank">
          </a>
          <a href="#" class="social fa fa-twitter">
          </a>
          <a href="https://plus.google.com/u/0/b/116916338082283282017/116916338082283282017/posts/p/pub" class="social fa fa-google-plus" target="_blank">
          </a>
          <a href="https://www.linkedin.com/company/3781339" class="social fa fa-linkedin" target="_blank">
          </a>
        </p>
        <!-- /social -->
      </div>
      <!-- /col #1 -->
      <!-- col #2 -->
      <div class="spaced col-md-8 col-sm-8 hidden-sm hidden-xs">
        <div class="row">
          <div class="col-md-4">
            <h4>Information
            </h4>
            <ul class="list-unstyled nobordered">
              <li>
                <a class="block" href="index.php">
                  <i class="fa fa-angle-right">
                  </i> Home
                </a>
              </li>
              <li>
                <a class="block" href="about-us.php">
                  <i class="fa fa-angle-right">
                  </i> About Us
                </a>
              </li>
              <li>
                <a class="block" href="mission-and-values.php">
                  <i class="fa fa-angle-right">
                  </i> Mission/Value
                </a>
              </li>
              <li>
                <a class="block" href="services.php">
                  <i class="fa fa-angle-right">
                  </i> Our Services
                </a>
              </li>
              <li>
                <a class="block" href="our-resources.php">
                  <i class="fa fa-angle-right">
                  </i> Our Resources
                </a>
              </li>
              <li>
                <a class="block" href="our-partners.php">
                  <i class="fa fa-angle-right">
                  </i> Our Partners
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <h4>
              <br/>
            </h4>
            <ul class="list-unstyled nobordered">
              <li>
                <a class="block" href="it-training.php">
                  <i class="fa fa-angle-right">
                  </i> IT Training
                </a>
              </li>
              <li>
                <a class="block" href="database-management.php">
                  <i class="fa fa-angle-right">
                  </i> Database Management
                </a>
              </li>
              <li>
                <a class="block" href="business-transformation.php">
                  <i class="fa fa-angle-right">
                  </i> Business Transformation
                </a>
              </li>
              <li>
                <a class="block" href="application-development.php">
                  <i class="fa fa-angle-right">
                  </i> Application Development
                </a>
              </li>
              <li>
                <a class="block" href="web-and-portal-development.php">
                  <i class="fa fa-angle-right">
                  </i> Web & Portal Development
                </a>
              </li>
              <li>
                <a class="block" href="consulting-services.php">
                  <i class="fa fa-angle-right">
                  </i> Consulting Services
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <h4>
              <br/>
            </h4>
            <ul class="list-unstyled nobordered">
              <li>
                <a class="block" href="approach.php">
                  <i class="fa fa-angle-right">
                  </i> Our Approach
                </a>
              </li>
              <li>
                <a class="block" href="news.php">
                  <i class="fa fa-angle-right">
                  </i> Latest News
                </a>
              </li>
              <li>
                <a class="block" href="industries.php">
                  <i class="fa fa-angle-right">
                  </i> Industries
                </a>
              </li>
              <li>
                <a class="block" href="#">
                  <i class="fa fa-angle-right">
                  </i> Testimonials
                </a>
              </li>
              <li>
                <a class="block" href="#">
                  <i class="fa fa-angle-right">
                  </i> Careers
                </a>
              </li>
              <li>
                <a class="block" href="contact-us.php">
                  <i class="fa fa-angle-right">
                  </i> Contact Us
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /col #2 -->
    </div>
  </div>
  <hr />
  <div class="copyright">
    <div class="container text-center fsize12">
      Powered by 
      <a href="http://#" target="_blank" title="" class="copyright">Amazing 7 Studios
      </a> &bull; All Right Reserved &copy; Dynamics Global. &nbsp;
      <a href="#" class="fsize11">Privacy Policy
      </a> &bull; 
      <a href="#" class="fsize11">Terms of Service
      </a>
    </div>
  </div>
</footer>
<!-- /FOOTER -->
<a href="#" id="toTop">
</a>
<script type="text/javascript" src="engine1/jquery.js">
</script>
<div id="myModal" class="reveal-modal">
  <h1>Apply Online
  </h1>
  <script type="text/javascript">
    function formValidator()
    {
      var name=document.getElementById('name');
      var email=document.getElementById('email');
      var contact_subject=document.getElementById('contact_subject');
      var message=document.getElementById('message');
      if(isname(name, "Please enter only letters for your Name"))
      {
        if(isemail(email, "Please Enter Your Valid Email"))
        {
          if(isnum(contact_subject, "Please Enter Your Valid Contact No."))
          {
            if(isname(message, "Please Enter Your Message"))
            {
              return true;
            }
          }
        }
      }
      return false;
    }
    function isname(elem, helpermsg)
    {
      var alphaExp = /^[A-Za-z\\-\\., \']+$/;
      if(elem.value.match(alphaExp))
      {
        return true;
      }
      else
      {
        alert(helpermsg);
        elem.focus();
        return false;
      }
    }
    function isemail(elem, helpermsg)
    {
      var emailexp=/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-Z0-9]{2,4}$/;
      if(elem.value.match(emailexp))
      {
        return true;
      }
      else
      {
        alert(helpermsg);
        elem.focus();
        return false;
      }
    }
    function isnum(elem, helpermsg)
    {
      var phexp=/^[0-9\\-\\., \']+$/;
      if(elem.value.match(phexp))
      {
        return true;
      }
      else
      {
        alert(helpermsg);
        elem.focus();
        return false;
      }
    }
  </script>
  <form action="e_process1.php" method="post" id="contactform_main" onsubmit="return formValidator()">
    <div class="row">
      <div class="form-group">
        <div class="col-md-6">
          <label>Full Name *
          </label>
          <input required type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
        </div>
        <div class="col-md-6">
          <label>E-mail Address *
          </label>
          <input required type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="form-group">
        <div class="col-md-12">
          <label>Phone No. *
          </label>
          <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="contact_subject" id="contact_subject">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="form-group">
        <div class="col-md-12">
          <label>Address
          </label>
          <input type="text" value="" data-msg-required="Please enter the your address." maxlength="100" class="form-control" name="address" id="address">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="form-group">
        <div class="col-md-12">
          <label>Message *
          </label>
          <textarea required maxlength="1000" data-msg-required="Please enter your message." rows="3" class="form-control" name="message" id="message">
          </textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <input class="hidden" type="text" name="captcha" id="captcha" value="" />
        <!-- keep it hidden -->
        <input type="submit" name="Submit" value="Submit" class="btn btn-primary btn-lg" id="contact_submit" />
      </div>
    </div>
  </form>
  <link rel="stylesheet" href="css/reveal.css">	
  <script type="text/javascript" src="js/jquery.reveal.js">
  </script>
  <a class="close-reveal-modal">&#215;
  </a>
</div>
<script type="text/javascript" src="js/scripts.js">
</script>
<script type="text/javascript" src="js/owl.carousel.js">
</script>
<script type="text/javascript" src="js/owl.carousel.min.js">
</script>
<script type="text/javascript" src="js/bootstrap.min.js">
</script>
</div>
</body>
</html>
