<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Contact Us :: Dynamics Global IT Solutions</title>
<script type="text/javascript">
function formValidator()
{
    var name=document.getElementById('name');
	var email=document.getElementById('email');
	
		if(isname(name, "Please enter only letters for your Name"))

		{
			if(isemail(email, "Please Enter Your Valid Email"))
			{
                if(isname(message, "Please Enter Your Message"))
    		{
						return true;
			}
			}
		}
	
return false;
}
function isname(elem, helpermsg)
{
	var alphaExp = /^[A-Za-z\\-\\., \']+$/;
	if(elem.value.match(alphaExp))
	{	
		return true;
	}
	else
	{
		alert(helpermsg);
		elem.focus();
		return false;
	}
}

function isemail(elem, helpermsg)
{
	var emailexp=/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-Z0-9]{2,4}$/;

	if(elem.value.match(emailexp))
	{	
		return true;
	}
	else
	{
		alert(helpermsg);
		elem.focus();
		return false;
	}
}
</script>

<?Php require("header.php"); ?>
<!-- PAGE TOP -->
			<section class="page-title img-responsive" style="background: url(images/contact-us.jpg) no-repeat 0px 0px;">
				<div class="container">

					<header>
						<h2><br/><!-- Page Title -->
							<!-- <strong>GET IN TOUCH</strong> Us -->
						</h2><!-- /Page Title -->

					</header>

				</div>			
			</section>
			<!-- /PAGE TOP -->
		

							<br/>
							
								<!-- /* <div class="col-md-9 col-sm-8">-->
						<!-- CONTENT -->
			<section>
				<div class="container">

					<div class="row">
					<!-- FORM -->
						<div class="col-md-8">
   <h2 style="background: #11a6cf;padding: 0 10px 0px;color: #FFF;font-size: 20px;font-weight: bold;">GET IN TOUCH</h2>
							<h3>Drop us a line or just say <strong><em>Hello!</em></strong></h3>

							<form action="e_process.php" method="post" id="contactform_main" >
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>Full Name *</label>
											<input required type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name111" id="name111">
										</div>
										<div class="col-md-6">
											<label>E-mail Address *</label>
											<input required type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email11" id="email11">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Subject</label>
											<input required type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="contact_subject11" id="contact_subject11">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Phone No.</label>
											<input required type="text" value="" data-msg-required="Please enter the Phone Number." maxlength="100" class="form-control" name="contact_phone11" id="contact_phone11">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Message *</label>
											<textarea required maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message11" id="message11"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input class="hidden" type="text" name="captcha" id="captcha" value="" /><!-- keep it hidden -->
										<input type="submit" name="Submit" value="Submit" class="btn btn-primary btn-lg" id="contact_submit" />
									</div>
								</div>
							</form>
<br/>
						</div>
						<!-- /FORM -->


						<!-- INFO -->
						<div class="col-md-4">

							<h2>Visit Us</h2>

							

							<div class="divider half-margins"><!-- divider --></div>

							<p>
								<span class="block"><strong><i class="fa fa-map-marker"></i> Address:</strong> 6270 McDonough Drive, Norcross, GA 30093</span>
								<span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> <a href="tel:678-720-4916">678-720-4916</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Fax:</strong> <a href="">866-326-1422</a></span>
								<span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="">admin@dygitsolutions.com</a></span>
							</p>

							<div class="divider half-margins"><!-- divider --></div>

						<!--	<h4 class="font300">Business Hours</h4>
							<p>
								<span class="block"><strong>Monday - Friday:</strong> 10am to 6pm</span>
								<span class="block"><strong>Saturday:</strong> 10am to 2pm</span>
								
							</p>
-->
						</div>
						<!-- /INFO -->

						

</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
						

			<!-- /BRANDS -->	
</div>

			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>