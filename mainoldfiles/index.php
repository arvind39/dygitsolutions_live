<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Home :: Dynamics Global IT Solutions</title>

<?Php require("header.php"); ?>
<div class="col-md-12">
            <div id="wowslider-container1">
			<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>

	           <div class="ws_images"><ul>
                   <li><img src="data1/images/banner1.jpg" alt="" title="" id="wows1_0"/></li>
                   <li><img src="images/nbanner.jpg" alt="" title="" id="wows1_1"/></li>
                   <li><img src="data1/images/bannern.jpg" alt="" title="" id="wows1_2"/></li>
				   <li><img src="data1/images/banner5.jpg" alt="" title="" id="wows1_3"/></li>
				   <li><img src="data1/images/banner6.jpg" alt="" title="" id="wows1_4"/></li>
               </ul></div>
	          <div class="ws_shadow"></div>
	
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
 </div>
</div>
<?php require("sidebar.php"); ?>
<div class="col-md-9">
                              <br/><h3>Welcome to <span style="color:#11a6cf;;font-weight:bold;">Dynamics Global</span></h3>
							<p class="lead">Dynamics Global IT Solutions is a global IT solutions and services provider employs a blended model of grander technology, domain and process expertise to increase our customers competitive advantage. </p>
							<br/>
								<div class="col-md-6 col-sm-8">
							<h4 class="htest">Why Us</h4>
						<p class="just1">	<img src="images/whyus.png" alt="" class="float-left bordered">Dynamics Global IT Solutions is an employee-based, customer-oriented Information Technology Services firm specializing in offering a wide range of IT Staffing Solutions to our growing customer base.<br/></p>

						</div>
						<div class="col-md-6 col-sm-8">
							<h4 class="htest">Our Services</h4>
							<p class="just1"><img src="images/img1.png" alt="" class="float-left bordered">Dynamics Global IT Solutions provide a full range of Information Technology staffing services to successful organizations nationwide. Our innovative recruiting strategies are tailored to the individual needs of our clients.</p>

						</div>

							
							
							
</div>
</div>
	<hr class="nomargin" />

<!-- BRANDS -->
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel();
});
</script>
		<!-- 				
<section class="brands">
				<div class="container">
			
					<!-- carousel 
					<div class="owl-carousel controlls-over" data-plugin-options='{"singleItem": false, "pagination": false, "navigation": true, "autoPlay": true}'>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
						<div>
							<img class="img-responsive" src="images/1.jpg" alt="">
						</div>
					</div><!-- /carousel 
				</div>
				
			</section>
			<!-- /BRANDS 
</div>
-->
			
			<!-- /CONTENT -->
<?php
require("footer.php");
?>