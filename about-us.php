<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>About Us :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <!-- PAGE TOP -->
    <section class="page_banner">
      <img src="./images/about_us.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>About Us
        </h2>		
      </div>
    </section>
    <!-- /PAGE TOP -->
    <section class="page_conntent py-5">
      <section class="about_sec1 pt-md-5 bp-4 mb-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="ltr_box pb-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-7">
                    <div class="pe-xl-5 pe-lg-4 mt-md-0 mt-4 ">
                      <div class="tilte medium_title">
                        <h2>Dynamic Global IT Solution At A Glance</h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions is a global IT solutions and services provider employs a blended model of grander technology, domain and process expertise to increase our customers competitive advantage. At Dynamics Global IT Solutions, our experts put IT to work for you, applying custom solutions to your business problems quickly, efficiently and cost-effectively yet by following PMI, ITIL and CMMI standards. We pride on traditions of excellence in engineering and service delivery tied with high employee and customer satisfaction levels. Our unique hybrid-shore delivery model provides close onsite interaction with customers and a strong process oriented offshore team. At all levels, our employees continually interact to provide a superior outsourcing experience to customers. 100 percent employee owned.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/about_img1.jpg" class="img-fluid">
                    </div>
                  </div>
                </div>
              </div>
							
							<div class="ltr_box pb-4 pt-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="pe-xl-5 pe-lg-4 mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Our Mission</h2> 
                      </div>
                      <div class="text_box">
                        <p>To be the company of choice to help our clients increase their competitive edge by delivering business solutions through information technology. We emphasize clients to attain their strategic business goals by delivering cost effective, value-driven integrated technology solutions to seamlessly manage their business processes and products.
                        </p>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
							<div class="ltr_box pb-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="pe-xl-5 pe-lg-4 mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Values</h2> 
                      </div>
                      <div class="text_box pb-2">
											<ul class="list_style">
													<li>Excellence and Quality in all we do.</li>
													<li>Service and Integrity in how we treat our clients, employees, candidates, investors, and suppliers.</li>
													<li>Ethical and Moral conduct at all times.</li>
													<li>Social Responsibility to communities we serve.</li>
													<li>Teamwork acknowledging that everyone has talent and skills that work together for excellence.</li>
													<li>Priorities resulting in a balanced life with faith, family, career, and personal development.</li>													
												</ul>
                      </div>                                     
                    </div>
                  </div>
                
                </div>
              </div>
							<div class="ltr_box">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="pe-xl-5 pe-lg-4 mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>100% Employee Owned</h2> 
                      </div>
                      <div class="text_box">
											<p>Dynamics Global IT Solutions is 100% employee owned. Dynamics Global IT Solutions employee owners are committed to the common goal of delivering high-quality solutions to our customers. When you deal with anyone at any level of our company, you are dealing with an owner who puts the highest value on maintaining Dynamics Global IT Solutions reputation of trust by exceeding your expectations. Our integrity as employee owners demands that we are so trust worthy, your confidence in our ability to deliver is assured.</p>
                      </div>                                     
                    </div>
                  </div>
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
	
    </section>

    </div>
  </div>
<!-- /CONTENT -->
<?php
require("footer.php");
?>
