<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Business Transformation :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/bussiness_transformation_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Business Transformation</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="business_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/bussiness_transformation_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Business Process Management</h2> 
                      </div>
                      <div class="text_box">
                        <p>Our team does the heavy lifting to inject new efficiencies into your operations. At every step, we strive for simplicity and apply creativity to improve alignment to strategy and cut costs - while keeping an eye fixed on the best end-state scenarios.</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>            
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Project & Program Management
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Maximize the business value of your technology investments with major improvements in decision making, project speed to market, cost and performance.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Information Security & IT Risk Management
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We help financial institutions and other regulated businesses protect their critical IT and information assets by providing information security management, data privacy, IT risk governance, and compliance management.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>            
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
