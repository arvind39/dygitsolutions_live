<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="Author" content="" />
<!-- mobile settings -->
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />
<!-- WEB FONTS -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/weather-icons.min.css" rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/style2.css" />
<link href="css/layout.css" rel="stylesheet" type="text/css" />
<link href="css/header-default.css" rel="stylesheet" type="text/css" />
<link href="css/footer-default.css" rel="stylesheet" type="text/css" />
<link href="css/revolution-slider.css" rel="stylesheet" type="text/css" />
<link href="css/essentials.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-3.4.0.min.js"></script>
<script src="js/bootstrap.bundle.min.js"  crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"  crossorigin="anonymous"></script>

</head>
<body>
  <header>
    <div class="header_top py-2">
      <div class="container d-flex align-items-center justify-content-between flex-md-row flex-column">
        <div class="left_box">
          <a href="mailto:admin@dygitsolutions.com">
            <svg class="me-1" width="13" height="11" viewBox="0 0 13 11" fill="#fff" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.7 0H1.3C0.585 0 0.00649999 0.585 0.00649999 1.3L0 9.1C0 9.815 0.585 10.4 1.3 10.4H11.7C12.415 10.4 13 9.815 13 9.1V1.3C13 0.585 12.415 0 11.7 0ZM11.7 2.6L6.5 5.85L1.3 2.6V1.3L6.5 4.55L11.7 1.3V2.6Z" fill="white"/>
            </svg>
            admin@dygitsolutions.com
          </a>
          <a href="tel:6787204916" class="ms-4">
            <svg class="me-1" width="13" height="13" viewBox="0 0 13 13" fill="#fff" xmlns="http://www.w3.org/2000/svg">
              <path d="M2.61444 5.62611C3.65444 7.67 5.33 9.33833 7.37389 10.3856L8.96278 8.79667C9.15778 8.60167 9.44667 8.53667 9.69944 8.62333C10.5083 8.89056 11.3822 9.035 12.2778 9.035C12.675 9.035 13 9.36 13 9.75722V12.2778C13 12.675 12.675 13 12.2778 13C5.49611 13 0 7.50389 0 0.722222C0 0.325 0.325 0 0.722222 0H3.25C3.64722 0 3.97222 0.325 3.97222 0.722222C3.97222 1.625 4.11667 2.49167 4.38389 3.30056C4.46333 3.55333 4.40556 3.835 4.20333 4.03722L2.61444 5.62611Z" fill="white"/>
            </svg>
            678-720-4916
          </a>
        </div>
        <div class="right_box d-none d-md-block">
          <a href="#" class="cta" data-bs-toggle="modal" data-bs-target="#myModal">
            <span>Apply Online
            </span>
            <svg width="13px" height="10px" viewBox="0 0 13 10">
              <path d="M1,5 L11,5">
              </path>
              <polyline points="8 1 12 5 8 9">
              </polyline>
            </svg>
          </a>
        </div>
      </div>
    </div>
    <div class="header_bottom py-2">
      <div class="container d-flex align-items-lg-center justify-content-between flex-lg-row flex-column">
        <a class="logo" href="index.php">
          <img src="images/logo.png" alt="" />
        </a> 
        <nav class="navbar navbar-expand-lg p-0 w-100">
          <div class="d-flex w-100 d-flex align-items-center justify-content-between">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main_menu">
              <div class="hamburger-toggle">
                <div class="hamburger">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </div>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="main_menu">
              <div class="mobile_menu_icon d-lg-none">
                
              </div>
               
              <ul class="navbar-nav mb-2 mb-lg-0">
              <li class="nav-item mobile_menu_icon d-lg-none">
              <a class="logo" href="index.php">
                  <img src="images/logo.png" alt="" />
                </a>
              </li>
                <li class="nav-item">
                  <a class="nav-link" href="index.php"> Home
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="about-us.php">About US
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="mission-and-values.php">Values
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="approach.php">Approach
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="news.php">News
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link dropdown dropdown-toggle" href="service.php" data-toggle="dropdown">Our Services
                  </a>
                  <ul class="dropdown-menu shadow">
                    <li>
                      <a class="dropdown-item" href="database-management.php">Database Management
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="consulting-services.php">Consulting Services
                      </a>
                    </li>
                    <li >
                      <a class="dropdown-item" href="application-development.php">Application Development
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="web-and-portal-development.php">Web & Portal Development
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="mobile-development.php">Mobile Development
                      </a>
                    </li>
                    <li >
                      <a class="dropdown-item" href="remote-dba.php">Remote DBA
                      </a>
                    </li>
                    <li class="dropdown-submenu">
                      <a href="it-training.php" class="dropdown-item dropdown-submenu-toggle ">IT Training & Coaching
                      </a>
                      <ul class="dropdown-menu dropdown-submenu shadow">
                        <li>
                          <a class="dropdown-item" href="corporate-training.php">Corporate Training
                          </a>
                        </li>
                        <li>
                          <a class="dropdown-item" href="online-training.php">Online Training
                          </a>
                        </li>
                      </ul>
                    </li>           
                    <li>
                      <a class="dropdown-item" href="business-transformation.php">Business Trasformation
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="our-resources.php">Our Resources
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="our-partners.php">Partners
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="industries.php">Industries
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="careers-opportunities.php">Careers
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="contact-us.php">Contact Us
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>
