<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Services :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/service_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Services
        </h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="service_sec1 pt-md-5 pt-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/service_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Our Services
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions, LLC provides a wide range of software services. You name it and we build it! For every client requirement varies. Dynamics Global IT Solutions, has carefully categorized the services from which client can easily identify the nature of service they want. Following are the various services offered by Dynamics Global IT Solutions that client can browse through.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4 pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered m-0">
                            <thead>
                              <tr class="table-info">
                                <th scope="col" colspan="2">Verticals
                                </th>				
                              </tr>
                            </thead>
                            <tbody>
                              <tr>      							
                                <td> Application Development </td>
                                <td>Healthcare</td>                               						
                              </tr>  
															<tr>      							
                                <td> Web and Portal Development </td>
                                <td>Manufacturing, Logistics & Distribution</td>                               						
                              </tr>
															<tr>      							
                                <td>Mobile Development  </td>
                                <td>Telecom/Technology</td>                               						
                              </tr>  	
															<tr>      							
                                <td>Testing Services</td>
                                <td>Education</td>                   						
                              </tr>	
															<tr>      							
                                <td>Offshore Development</td>
                                <td>Energy & Utilities</td>                   						
                              </tr>
															<tr>      							
                                <td>Remote Database Administration</td>
                                <td>RealEstate</td>                   						
                              </tr>
															<tr>      							
                                <td>Enterprise Applications</td>
                                <td></td>                   						
                              </tr>	
															<tr>      							
                                <td>Managed Services</td>
                                <td></td>                   						
                              </tr>
															<tr>      							
                                <td>Technology Consulting</td>
                                <td></td>                   						
                              </tr>				
                            </tbody>
                          </table>                       
                      </div>
                    </div>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
