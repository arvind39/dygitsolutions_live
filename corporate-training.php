<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Corporate Training :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/corporate_training_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Corporate Training </h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="corporate_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-4 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/corpoate_training_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-xl-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Corporate Training Courses
                        </h2> 
                      </div>
                      <ul class="list_style column_count_2 pt-md-4">
                      <li>JAVA</li>
                        <li>.NET</li>
                        <li>Siebel CRM</li>	
                        <li>OBIEE
                        </li>
                        <li>Qlikview
                        </li>	
                        <li>Pega
                        </li>
                        <li>Site minder
                        </li>	
                        <li>SAP ABAP
                        </li>
                        <li>WEBDYNPRO(JAVA & ABAP)
                        </li>	
                        <li>SAP BO
                        </li>
                        <li>SAP BASIS
                        </li>	
                        <li>SAP SD, MM,FiCO
                        </li>
                        <li>Oracle 11i Financial, Apps technical
                        </li>													
                        <li>Mainframes
                        </li>
                        <li>Informatica
                        </li>
												<li>BUSINESS OBJECTS
                        </li>
                        <li>Cognos, Cognos TM1
                        </li>	
                      </ul>                        
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4">                     
                      <div class="text_box mt-3 mt-md-0">
                        <p>For Online Training courses 
                          <a href="contact-us.php">Send Inquiry
                          </a> or Application to: 
                          <a href="info@dygitsolutions.com">info@dygitsolutions.com
                          </a>
                        </p>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
