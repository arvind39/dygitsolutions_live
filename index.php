<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Home :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="home_sec1">
      <div class="main_slider">
        <div class="owl-carousel owl-theme" id="banner_slider">
          <div class="item" data-dot="<button>01</button>">
            <img src="images/new1.jpg" alt="images not found">
            <div class="cover">
              <div class="container">
                <div class="row">
                  <div class="col-md-8">
                    <div class="header-content">
                      <h4>IT Solutions
                      </h4>
                      <h2>Increse visibility. Generate relevant, quality traffic. Reduce advertising spend.
                      </h2>
                      <a href="application-development.php" class="cta cta_blue">
                        <span>Read More
                        </span>
                        <svg width="13px" height="10px" viewBox="0 0 13 10">
                          <path d="M1,5 L11,5">
                          </path>
                          <polyline points="8 1 12 5 8 9">
                          </polyline>
                        </svg>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item" data-dot="<button>02</button>">
            <img src="images/new3.jpg" alt="images not found">
            <div class="cover">
              <div class="container">
                <div class="row">
                  <div class="col-md-8">
                    <div class="header-content">
                      <h4>Technology Expertise 
                      </h4>
                      <h2>We successfully design, and develop application products
                      </h2>
                      <a href="services.php" class="cta cta_blue">
                        <span>Read More
                        </span>
                        <svg width="13px" height="10px" viewBox="0 0 13 10">
                          <path d="M1,5 L11,5">
                          </path>
                          <polyline points="8 1 12 5 8 9">
                          </polyline>
                        </svg>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item" data-dot="<button>03</button>">
            <img src="images/new2.jpg" alt="images not found">
            <div class="cover">
              <div class="container">
                <div class="row">
                  <div class="col-md-8">
                    <div class="header-content">
                      <h4>Corporate Trainings
                      </h4>
                      <h2>Outcome bsed training - courses designed by industry experts, delivered by experienced trainers</h2>
                      <a href="corporate-training.php" class="cta cta_blue">
                        <span>Read More
                        </span>
                        <svg width="13px" height="10px" viewBox="0 0 13 10">
                          <path d="M1,5 L11,5">
                          </path>
                          <polyline points="8 1 12 5 8 9">
                          </polyline>
                        </svg>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="home_sec2 py-5 my-md-5 my-1 ">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="home_about rtl">
              <div class="row align-items-lg-center">
                <div class="col-md-7">
                  <div class="pe-xl-5 pe-lg-4 mt-md-0 mt-4 ">
                    <div class="tilte">
                      <h2>Welcome to Dynamics Global</h2> 
                    </div>                                     
                    <p>Dynamics Global IT Solutions is a global IT solutions and services provider employs a blended model of grander technology, domain and process expertise to increase our customers competitive advantage.
                    </p>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="">
                    <img src="images/img1.jpg" class="img-fluid">
                  </div>
                </div>
              </div>
            </div>
            <div class="home_about mt-md-5 ltr pt-4 pt-sm-0">
              <div class="row align-items-lg-center flex-sm-row flex-column-reverse">
                <div class="col-md-5">
                  <div class="">
                    <img src="images/img2.jpg" class="img-fluid">
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="ps-xl-5 ps-lg-4 mt-md-0 text-md-end mt-4 ">
                  <div class="tilte t_right justify-content-end">
                    <h2>Why Us</h2> 
                  </div>                 
                    <p>Dynamics Global IT Solutions is an employee-based, customer-oriented Information Technology Services firm specializing in offering a wide range of IT Staffing Solutions to our growing customer base.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="home_sec3 pb-5 mb-md-4 mb-0">   
      <div class="container">
        <div class="tilte">
          <h2>Our Services</h2>
        </div>
       <hr class="custom_hr">
        <div class="row">
          <div class="col-md-12">
            <div class="service_part pt-4 px-4 px-sm-0">           
              <div class="row">
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="database-management.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">                                    
                    <img src="images/icon/database_management 1.png">
                    <h5 class="mt-3">Database Management
                    </h5>                 
                  </a> 
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="consulting-services.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/consult-g.png">
                    <h5 class="mt-4">Consulting Services
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="application-development.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/application-development.png">
                    <h5 class="mt-4">Application Development
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="web-and-portal-development.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/web-development.png">
                    <h5 class="mt-4">Web & Portal Development
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="mobile-development.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/mobile-app.png">
                    <h5 class="mt-4">Mobile Development
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="remote-dba.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/remote-dba 1.png">
                    <h5 class="mt-4">Remote DBA
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="it-training.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/it-training.png">
                    <h5 class="mt-4">IT Training & Coaching
                    </h5>
                  </a>
                </div>
                <div class="col-lg-3 col-md-6 mb-6 mb-4">
                  <a href="business-transformation.php" class="service_box d-flex flex-column align-items-center justify-content-center py-xl-5 py-md-4 py-5 px-3">
                    <img src="images/icon/remote-dba 1.png">
                    <h5 class="mt-4">Business Transformation
                    </h5>
                  </a>
                </div>
                <div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
   
    <?php
      require("footer.php");
    ?>
