<?php
ob_start();
function spamcheck($field)
{
    $field = filter_var($field, FILTER_SANITIZE_EMAIL);
	if(filter_var($field, FILTER_VALIDATE_EMAIL))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

if(isset($_POST['Submit']))
{
	$f_name = (isset($_POST['name'])) ? trim($_POST['name']) : '';
	$c_e = (isset($_POST['email'])) ? trim($_POST['email']) : '';
    $c_tye = (isset($_POST['contact_subject'])) ? trim($_POST['contact_subject']): '';
	  $c_add = (isset($_POST['address'])) ? trim($_POST['address']) :'';
    $c_msg = (isset($_POST['message'])) ? trim($_POST['message']) :'';
		$errors = array();
	//make sure manditory fields have been entered

	if(empty($f_name))
	{
		$errors[] = 'Please Enter Your Name';
	}
	if(empty($c_e))
	{
		$errors[] = 'Please Enter Your Email Address';
	}
	

	if(count($errors)>0)
	{
		echo '<p><strong>Unable to process.</strong></p>';
		echo '<p>Please Fix the following: </p>';
		echo '<ul>';
			foreach($errors as $error)
			{
				echo '<li>' . $error . '</li>';
			}
		echo '</ul>';
		header('Refresh: 4; URL=http://dygitsolutions.com/');
		
	}
	else
	{
		$mailcheck = spamcheck($_REQUEST['email']);
		if($mailcheck==FALSE)
		{
			echo "Invalid Email Address";
		}
		else
		{
			$subject ="Message from Dynamics Global(Apply Online)";
		$to = "info@dygitsolutions.com";
			$message = "MESSAGE FROM DYNAMICS GLOBAL(APPLY ONLINE)<br><br>";
			$message .= "<br>Customer Name: $f_name<br>";
			$message .= "Customer Email: $c_e<br><br>";
			$message .= "Phone No. : $c_tye<br><br>";
			$message .= "Address. : $c_add<br><br>";
			$message .= "Customer Message: $c_msg<br>";
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.$_REQUEST['email'].''. "\r\n";	
			
			mail($to,$subject,$message,$headers);
			?> <script type="text/javascript">alert("Thank you for contacting Dynamics Global. We have received your query. We will get back to you as soon as possible.");</script> <?php
			header('Refresh:0.5, URL=http://dygitsolutions.com/');		}
	}

}
else
{
	?> <script type="text/javascript">alert("You are not authorised for this page");</script> <?php
		header('Refresh:0.5, URL=http://dygitsolutions.com/');	
	}

ob_flush();
?>