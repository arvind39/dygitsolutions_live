<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Contact Us :: Dynamics Global IT Solutions
    </title>
    <script type="text/javascript">
      function formValidator()
      {
        var name=document.getElementById('name');
        var email=document.getElementById('email');
        if(isname(name, "Please enter only letters for your Name"))
        {
          if(isemail(email, "Please Enter Your Valid Email"))
          {
            if(isname(message, "Please Enter Your Message"))
            {
              return true;
            }
          }
        }
        return false;
      }
      function isname(elem, helpermsg)
      {
        var alphaExp = /^[A-Za-z\\-\\., \']+$/;
        if(elem.value.match(alphaExp))
        {
          return true;
        }
        else
        {
          alert(helpermsg);
          elem.focus();
          return false;
        }
      }
      function isemail(elem, helpermsg)
      {
        var emailexp=/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-Z0-9]{2,4}$/;
        if(elem.value.match(emailexp))
        {
          return true;
        }
        else
        {
          alert(helpermsg);
          elem.focus();
          return false;
        }
      }
    </script>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/contact_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Contact Us
        </h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="contact_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="address_box">
                <div class="tilte medium_title pb-md-5 pb-4">
                  <h2>Visit Us
                  </h2> 
                </div>
                <ul>
                  <li>
                    <span>
                      <i class="fa fa-map-marker">
                      </i> 
                      <b>Address:
                      </b>
                    </span>
                    <p>7094 Peachtree Industrial Blvd # 135 <br>Norcross, GA 30071
                    </p>
                  </li>
                  <li>
                    <span>
                      <i class="fa fa-phone">
                      </i> 
                      <b>Phone:
                      </b>
                    </span>
                    <a href="tel:6787204916">678-720-4916
                    </a>
                  </li>
                  <li>
                    <span>
                      <i class="fa fa-fax">
                      </i> 
                      <b>Fax:
                      </b>
                    </span>
                    <p>866-326-1422
                    </p>
                  </li>
                  <li>
                    <span>
                      <i class="fa fa-envelope">
                      </i> 
                      <b>Email:
                      </b>
                    </span>
                    <a href="mailto:admin@dygitsolutions.com">admin@dygitsolutions.com
                    </a>
                  </li>
                </ul>
              </div>             
            </div>
            <div class="col-md-6">
              <div class="form_box">
                <div class="tilte medium_title pb-md-4 pt-md-0 pt-4">
                  <h2>Get In Touch
                  </h2> 
                </div>                
                <form action="e_process.php" method="post" id="contactform_main" class="p-0">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group my-3">
                        <input type="text" id="name111" class="input_field" name="name111" maxlength="100" placeholder="Full Name" required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group my-3">
                        <input type="email" maxlength="100" class="input_field" name="email11" id="email11" placeholder="Email Address" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group my-3">
                        <input type="text"  maxlength="100" class="input_field" name="contact_subject11" id="contact_subject11" placeholder="Subject" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group my-3">
                        <input type="text" class="input_field" name="contact_phone11" id="contact_phone11" pattern="\d{10}" title="Only 10 digits mobile number" placeholder="Phone Number" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group my-3">											
                        <textarea  minlength="25" maxlength="5000" rows="4" class="input_field_textarea input_field"  name="message11" id="message11" placeholder="Your Message" required>
                        </textarea>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group my-3">
                          <input class="d-none" type="text" name="captcha" id="captcha" value="" />
                          <!-- keep it hidden -->
                          <input type="submit" name="Submit" value="Submit" class="submit_btn" id="contact_submit" />
                        </div>
                      </div>                      
                    </div>  
                  </div>               
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
