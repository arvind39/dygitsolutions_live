<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>IT Training & Coaching:: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/it_training_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>IT Training</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="it_training_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/it_training_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>IT Training & Coaching</h2> 
                      </div>
                      <div class="text_box">
                        <p>To begin with a more exclusive simulation IT training, Dynamics Global IT Solutions offer people to take party in all the possible courses it offers.The courses include training for Beginners, advanced professionals and educators. The courses are held at the training centers or at their main office. The training offered to people are crafted to suit their needs and skills.Every training event is devised to be creative, engaging and challenging.</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="ltr_box pb-md-5">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Corporate Training</h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions is ready to identify and overcome those difficult issues that limit the organization's potential & establish a thriving model that enables it to conceptualize & realize organization review & change initiatives.</p>
												<a href="corporate-training.php" class="cta cta_dark_blue">
													<span>Read More</span>
													<svg width="13px" height="10px" viewBox="0 0 13 10">
														<path d="M1,5 L11,5">
														</path>
														<polyline points="8 1 12 5 8 9">
														</polyline>
													</svg>
                      	</a>
											</div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Online Training</h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions is a pioneer in the field of learning, delivering highly effective content through innovative technology. </p>
												<a href="online-training.php" class="cta cta_dark_blue">
													<span>Read More</span>
													<svg width="13px" height="10px" viewBox="0 0 13 10">
														<path d="M1,5 L11,5">
														</path>
														<polyline points="8 1 12 5 8 9">
														</polyline>
													</svg>
                      	</a>
											</div>                                     
                    </div>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
