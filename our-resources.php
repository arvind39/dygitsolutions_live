<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Resources :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/resources_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Resources</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="our_resource_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/resource_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Our Resources</h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions has the resources and expertise that deliver maximum results for you. Whether you need temporary help, temp-to-full-time hire or full-time hires, we can quickly and effectively place talented people in the right jobs and ensure that you operate at maximum productivity.

												</p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Industry professionals
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Our staffing professionals have years of experience in the industries they serve and an in-depth understanding of the workforce and the issues that most affect your business performance.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Timeliness
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We are 100% dedicated to finding and placing talented workers. With a wide network of professionals, we are able to quickly provide the most qualified candidates when and where you need them.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">									
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Flexibility
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Our vast network includes professionals seeking a range of work arrangements. Whether you need a temporary employee for a project, a full-time addition to your staff, or anything in between, we can help. We are nimble enough to change as rapidly as your requirements do.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
							<div class="ltr_box">
                <div class="row align-items-lg-center">									
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Customer service
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We gain an in-depth understanding of your business needs to ensure we deliver both exceptional service and results.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
