<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Career Opportunities :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/carrers_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Career Opportunities
        </h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="careers_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">              
              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Front End UI Developer (AngularJS)
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple locations
                                </th>			
                              </tr>
                            </thead>
                            <tbody>
                             
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>Long Contract</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>Front-End Lead UI Developer who has proven skills in UI and JavaScript development using AngularJS. This resource will provide the UI engineering team with technical leadership. This resource will be experienced in developing Web applications using HTML, CSS, JavaScript, AngularJS, JQuery, Bootstrap, AJAX, XML, Core Java, and J2EE. Good working knowledge of GIT, and knowledge of REST, Spring, and Hibernate would be a plus. Onsite – NO REMOTE!</td>                               						
                              </tr>
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: JAVA Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>Long Contract</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Develop and unit test Java code as a result of new business requirements and solution specifications</li>
                                    <li>Develop Java fixes as a result of production tickets</li>
                                    <li>Provide development support for system, user, and performance testing</li>
                                    <li>Create and update design and code construction documents as a result of new code, code changes, and code fixes</li>                                												
                                  </ul>
                                </td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Required skills</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Minimum 5 years of full-time Java development experience</li>
                                    <li>Ability to quickly pick up new concepts and technologies</li>
                                    <li>Ability to effectively manage multiple tasks in a changing environment</li>
                                    <li>Good communications skills and successful experience explaining technical details to clients.</li>
                                  </ul>
                                </td>                               						
                              </tr>
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: SQL Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>1-2 year contract</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Must-haves</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Strong understanding of relational database concepts and vision, with proven ability in writing complex and efficient SQL queries.</li>
                                    <li>Experience creating, maintaining and deploying SQL Server Integration Services (SSIS) packages.</li>
                                    <li>5+ years SQL Server Reporting Services development experience.</li>
                                    <li>Experience using Team Foundation Server for Source Control.</li>
                                    <li>SQL 2008 R2/ SQL 2012 experience</li>
                                    <li>Strong oral and written communication skills.</li>                                 												
                                  </ul>
                                </td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Plusses</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Experience in developing PowerPivot and PowerView reports.</li>
                                    <li>Experience with Microsoft Visual Studio SQL Server Data tools.</li>
                                    <li>Experience with data modeling tools (i.e., ERwin, ER Studio).</li>                                    
                                  </ul>
                                </td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Day-to-Day</td>
                                <td>
                                Looking for a Database / Report Developer for one of our clients in the Houston area. This person will be part of the Technical Team and play an active role in database and reporting related development activities. Activities will include: Writing SQL scripts/ stored procedures/ SQL Server Integration Services (SSIS packages) to load test/seed data from spreadsheets in SQL Server database, designing and developing reports in Microsoft SQL Server Reporting Services (SSRS) sourced by stored procedures on SQL-Server 2012 databases to source report data. The developer should have a very good understanding of relational database design and in-depth skills in TSQL programming.
                                </td>                               						
                              </tr>
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: UI Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">San Francisco, CA
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  Understanding of Web Technologies currently used in the Industry (Http protocol, Https, Html 4/5, Css 2/3, Javascript, Ajax, Json) Very strong understanding of how Browser works with Html, Javascript and Css to manage (parse, render, manipulate) web pages and cross browser compatibility issues.<br><br>Understanding of semantic markup, css based design and non-intrusive event handling Demonstrable understanding of Javascript as a language (oop, prototype, dynamic typing, event mechanism etc.) and have an opinion on its strengths, weaknesses, caveats, pitfalls and best practices Strong experience in developing MVC architecture based Single Page Applications using framework Backbone, Meteor, Ember etc.<br><br>Experience with must-have Javascript libraries like JQuery, Underscore, Moment, JQuery UI etc.<br><br>Experience with developing reusable UI Plugins/Components using JQuery and JQuery Widgets Understanding of how Css works (selectors, cascading, box model) Experience in writing Css using Pre-processors like Less or Sass Ability realize a given UI design as an image into a webpage built using Html/Css
                                </td>                               						
                              </tr>
                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: DataStage Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months plus extension possible</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Have good experience with oracle PL/SQL (packages, procedures, functions) with ETL</li>
                                    <li>Minimum 5 years on DataStage 8.1 or 8.0, Redwood scheduler.</li>
                                    <li>Good problem solving skills and client facing can work independently with client requirements, delivery and provide status reports.</li>
                                  </ul>
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Teradata Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Candidate should have strong Data Warehousing / BI background / SQL knowledge.</li>
                                    <li>Candidates with good Experience in Telecom/Cable Domain will be given preference</li>
                                    <li>Candidate should have good knowledge on loading data from various data sources and legacy systems into Teradata production and development warehouse using BTEQ, FASTEXPORT, MULTI LOAD, and FASTLOAD utilities.</li>
                                    <li>In-depth expertise in the Teradata cost based query optimizer, identified potential bottlenecks with queries from the aspects of query writing, skewed redistributions, join order, optimizer statistics, physical</li>
                                    <li>Design considerations (PI/USI/NUSI/JI etc) etc. In-depth knowledge of Teradata Explain and Visual Explain to analyze and improve query performance</li>
                                    <li>Extensive use of transformations like Aggregate, Filter, Join, Expression, Lookup, Update Strategy, Expressions, Sequence Generator Transformations. Used debugger to test and fix mapping.</li>
                                    <li>Performance tuned the workflows by identifying the bottlenecks in targets, sources, mappings, sessions and workflows and eliminated them</li>
                                    <li>Multiload, BTEQ, created & modified databases, performed capacity planning, allocated space, granted rights for all objects within databases, etc.</li>
                                    <li>Used Teradata Administrator and Teradata Manager Tools for monitoring and control the system.</li>
                                    <li>Use of various Teradata utilities.</li>
                                    <li>Candidate should be flexible in working onshore/offshore model.</li>
                                  </ul>
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
              
              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: SFDC SR Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                Direct experience with one of SFDC Sales Cloud, Service Cloud, Knowledge (application design, architecture and development using Salesforce.com), Salesforce.com communities. More than 5 years of Salesforce experience required<br><br>
                                Strong SFDC knowledge and Integration components for SFDC ( for example, SFDC APIs(SOAP, REST, BULK APIs), Apex controllers, Apex Web Services, Apex callout, outbound messaging, SFDC data loaders, etc.)<br><br>
                                Strong technical foundation including; Advanced structured programming - APEX, Force.com, Java, etc., understanding of RDMS concepts and structures, knowledge of SQL, structured system analysis and design methods, etc.<br></br>
                                Understands complex ETL and data migration practices and has performed at least 1 complex data migration to Salesforce.com.  
                              </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Informatica Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Strong Informatica knowledge and experience.</li>
                                    <li>Strong shell scripting experience.</li>
                                    <li>Strong Communication skills, written and verbal.</li>
                                    <li>Have done full project life cycle Informatica implementation.</li>
                                    <li>strong hands on leader and team player with capability to engage deployment and site team towards the common goals.</li>
                                  </ul>
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: MSBI Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Multiple
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Primary Skills</td>
                                <td>MSBI (SSRS/SSAS/SSIS) /Data Warehousing / BI background / SQL knowledge.</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Candidate should have strong MSBI (SSRS/SSAS/SSIS) /Data Warehousing / BI background / SQL knowledge.</li>
                                    <li>Candidates with good Experience in Telecom/Cable Domain will be given preference</li>
                                    <li>Candidate should be proficient in translating business requirements into technical requirements for development team and create document like technical requirement document, S2T (Source to Target mappings</li>
                                    <li>Candidates with strong analytical ability who have experience in Data analysis to identify root cause of issues will be preferred</li>
                                    <li>Candidate should have experience working with business users and have very good communication skills</li>
                                    <li>Candidate should be flexible in working onshore/offshore model.</li>
                                  </ul>                                  
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Cognos Developer
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Minneapolis, MN
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Primary Skills</td>
                                <td>Development, Maintenance, Support, Performance Tuning, Cognos Reports, Oracle</td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>Development, Maintenance, support, Performance tuning of Cognos reports.</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Responsibilities and Duties</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Strong hands on in Cognos report development, performance tuning, framework manager</li>
                                    <li>Strong hands on in oracle database SQL tuning</li>
                                    <li>Strong DW/BI concepts</li>
                                    <li>Scheduling, migration of reports</li>
                                  </ul>                                  
                                </td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Required Skills</td>
                                <td>
                                  <ul class="list_style">
                                    <li>At least 6 years of experience in Cognos</li>
                                    <li>At least 6 years of experience in Datawarehosuing</li>
                                    <li>Ability to communicate clearly</li>
                                  </ul>
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Ruby on Rails
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Minneapolis, MN
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>6 Months</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>
                              <tr>      							
                                <td class="fw-bold no_wrap">Primary Skills</td>
                                <td>UI skills including Javascript, HTML, jQuery</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                  <ul class="list_style">
                                    <li>Good development experience in Ruby on Rails</li>
                                    <li>Ability to reengineer Open Source components from Ruby on Rails and redesign the components</li>
                                    <li>Good UI skills - Javascript, HTML, CSS</li>
                                    <li>Ability to do feasibility analysis on the Opensource products and provide suggestions.</li>
                                    <li>Good communication skills</li>
                                  </ul> 
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

              <div class="ltr_box pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Job Title: Datawarehouse
                        </h2> 
                      </div>
                      <div class="text_box job_box mt-3">

											<div class="table_box table-responsive mb-4 mb-md-0">                       
                          <table class="table table-bordered">
                            <thead>
                              <tr class="table-info">
                                <th scope="col">Location
                                </th>	
																<th scope="col">Minneapolis, MN
                                </th>			
                              </tr>
                            </thead>
                            <tbody>                           
															<tr>      							
                                <td class="fw-bold no_wrap">Duration</td>
                                <td>12 - 18 month contract plus extensions</td>                               						
                              </tr>
															<tr>      							
                                <td class="fw-bold no_wrap">Payroll</td>
                                <td>Only W2 or 1099</td>                               						
                              </tr>                              
															<tr>      							
                                <td class="fw-bold no_wrap">Job Description</td>
                                <td>
                                I need a strong Project Manager who also has experience constructing/executing SQL queries. This person should also have a strong understanding of SQL Server, Oracle, and/or DB2 databases. Must have knowledge of project management tools and software packages and will have experience with full life cycle project development and execution. Any Big Data project experience is a big plus.
                                </td>                               						
                              </tr>                             
                            </tbody>
                          </table>                       
                      </div>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
