<div class="side_bar">
  <h3>
    <h4 class="htest">OUR SERVICES
    </h4>
    <!-- SIDE NAV -->
    <ul class="ca-menu">
      <li>
        <a href="database-management.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Database Management
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="consulting-services.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Consulting Services
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="application-development.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Application Development
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="web-and-portal-development.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Web & Portal Development
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="mobile-development.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Mobile Development
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="remote-dba.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Remote DBA
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="it-training.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">IT Training & Coaching
            </h2>
          </div>
        </a>
      </li>
      <li>
        <a href="business-transformation.php">
          <span class="ca-icon">>
          </span>
          <div class="ca-content">
            <h2 class="ca-main">Business Transformation
            </h2>
          </div>
        </a>
      </li>
    </ul>
</div>