<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Values :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <!-- PAGE TOP -->
    <section class="page_banner">
      <img src="./images/value_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Mission and values
        </h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="mission_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/value_img1.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Values
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions is committed to helping our customers meet their goals and achieve their missions. In line with our commitment, our core values are the principles that define our behavior. They shape our strategy and set our priorities. Internally, they guide our culture, and externally, they are fundamental to our success. Our values define how we interact with one another, our customers and our partners. They influence our strategic planning as well as our day-to-day decision making.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4  pt-md-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Integrity - We commit to the highest ethical standards in all that we do
                        </h2> 
                      </div>
                      <div class="text_box mt-3 mt-md-0">
                        <ul class="list_style">
                          <li>We conduct our activities honestly and ethically.
                          </li>
                          <li>We earn our reputation as a highly ethical company through the integrity of our decisions and actions.
                          </li>													
                        </ul>
                      </div>                                     
                    </div>
                  </div>                
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Respect - We encourage diversity of culture, background, experience, thoughts and ideas
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We demonstrate respect for our colleagues and customers through professionalism in our words and actions. We benefit as a team from our differences, where diversity contributes to innovative ideas and drives better results for our customers.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>100% Employee owned
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Dynamics Global IT Solutions is 100% employee owned. Dynamics Global IT Solutions employee owners are committed to the common goal of delivering high-quality solutions to our customers. When you deal with anyone at any level of our company, you are dealing with an owner who puts the highest value on maintaining Dynamics Global IT Solutions reputation of trust by exceeding your expectations. Our integrity as employee owners demands that we are so trust worthy, your confidence in our ability to deliver is assured.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="ltr_box pb-md-4">
                <div class="row align-items-lg-center">									
                  <div class="col-md-12">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte small_title">
                        <h2>Trust - We follow through on commitments to our teammates and customers
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We earn our customer's trust by providing quality service and delivering on our promises, striving for continuous improvement. We maintain open and honest communications with our fellow employees, partners and customers.
                        </p>
                        <p>Teamwork - We are "one team, one fight," working together to achieve our goals
                        </p>
                        <p>We utilize resources, best practices and capabilities across our company to meet our customer's requirements. We contribute to the efficiency, growth and profitability of our company through dedication to teamwork.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="rtl_box pb-md-4 pt-4 pt-md-0">
                <div class="row align-items-lg-center flex-md-wrap flex-wrap-reverse">
                  <div class="col-md-7 pe-xl-5 pe-lg-4">
                    <div class="mt-md-0 mt-4 ">
                      <div class="tilte medium_title">
                        <h2>Mission - We make business decisions that are first and foremost based on achieving customer mission success
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>We provide services and solutions that are relevant, innovative, timely and affordable-consistently exceeding customer expectations. We are proud to do meaningful work and help our customers achieve their goals in service to their constituents.
                        </p>
                      </div>                                     
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/value_img2.jpg" class="img-fluid">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<!-- /CONTENT -->
<?php
require("footer.php");
?>
