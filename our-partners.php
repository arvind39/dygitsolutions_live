<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	
<html> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <title>Partners :: Dynamics Global IT Solutions
    </title>
    <?Php require("header.php"); ?>
    <section class="page_banner">
      <img src="./images/partners_banner.jpg" class="img-fluid">
      <div class="page_title">			
        <h2>Our Partners</h2>		
      </div>
    </section>
    <section class="page_conntent py-5">
      <section class="our_partners_sec1 pt-md-5 pt-4 bp-4 mb-md-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="rtl_box pb-md-5 pb-2">
                <div class="row align-items-lg-center">
                  <div class="col-md-5">
                    <div class="img_box">
                      <img src="images/partners_img.jpg" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="ps-xl-5 ps-lg-4 mt-md-0 mt-5 ">
                      <div class="tilte medium_title">
                        <h2>Become Our Partners
                        </h2> 
                      </div>
                      <div class="text_box">
                        <p>Thank you for your interest in becoming Our Partner with Dynamics Global IT Solutions. Ensuring the satisfaction of our end-user customers and serving our partners is a priority for Dygits LLC. The success of our our Partners is also our success and we have built our organization and programs to help guarantee that success.
                        </p>
												<p class="fst-italic fs-5 fw-light">" Request additional information by filling out our Partner inquiry form. "
                        </p>
                      </div>                                     
                    </div>
                  </div>
                </div>
              </div>           
            </div>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
<?php
require("footer.php");
?>
